let test_btn = document.getElementsByClassName('nav-link');
let h1 = document.getElementsByTagName('h1');
let bg = document.querySelector('body');
const colors = ['#ffff00','#ff0000','#ffffff','#12dede','#cdcdcd','#721c24','#0c5460','#155724','#1d1557'];

for(let i = 0; i < test_btn.length; i++){
  test_btn[i].addEventListener('click', function(b){
    let color = this.getAttribute('data-color');
    color = color === 'random' ? colors[Math.floor(Math.random() * 5)] : color;
    bg.style.backgroundColor = color;
    h1[0].innerHTML = `HEX Color ${color}`;
  });
}
